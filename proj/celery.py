#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# vim: sts=4 ts=4 sw=4 et ai

from __future__ import absolute_import

from celery import Celery
import os, subprocess, sys

def get_config_yaml(path_config=os.path.dirname(os.path.realpath(__file__)) + "/config.yaml", name_config="celery-cron"):
    import yaml
    from yaml import Loader, SafeLoader

    def construct_yaml_str(self, node):
        # Override the default string handling function 
        # to always return unicode objects
        return self.construct_scalar(node)

    Loader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)
    SafeLoader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)

    try:
        f = open(path_config)
    except:
        return dict({'user': 'guest', 'password': 'guest', 'host': 'localhost'})

    # use safe_load instead load
    dataMap = yaml.load(f)[name_config]
    f.close()
    return dataMap

app = Celery('proj',
             broker='amqp://{user}:{password}@{host}'.format(**get_config_yaml()),
             backend='amqp://{user}:{password}@{host}'.format(**get_config_yaml()),
             include=['proj.tasks'])

# Optional configuration, see the application user guide.
app.conf.update(
#   CELERY_TASK_RESULT_EXPIRES=3600,
#   BROKER_URL = 'amqp://guest@localhost//',
#   CELERY_RESULT_BACKEND = 'amqp://guest@localhost//',
#   CELERY_TASK_SERIALIZER = 'json',
#   CELERY_RESULT_SERIALIZER = 'json',
#   CELERY_ACCEPT_CONTENT=['json'],
#   CELERY_TIMEZONE = 'Europe/Oslo',
#   CELERY_ENABLE_UTC = True,
#   CELERYBEAT_SCHEDULE_FILENAME = "/tmp/celerytest.db"
#   #CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
    CELERY_CREATE_MISSING_QUEUES = "Disabled"
)

from kombu import Exchange, Queue


CELERY_DEFAULT_QUEUE = 'celery'
CELERY_QUEUES = (
    Queue('celery', Exchange('default'), routing_key='default'),
    Queue('PrioHigh', Exchange('High'), routing_key='High'),
    Queue('PrioAver', Exchange('Aver'), routing_key='Aver'),
    Queue('PrioLow', Exchange('Low'), routing_key='Low'),
)

CELERY_ROUTES = {
    'RHigh': {'queue': 'PrioHigh', 'routing_key': 'High'},
    'RAver': {'queue': 'PrioAver', 'routing_key': 'Aver'},
    'RLow': {'queue': 'PrioLow', 'routing_key': 'Low'},
}

# set the default Django settings module for the 'celery' program.


if __name__ == '__main__':
    app.start()
