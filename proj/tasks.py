from __future__ import absolute_import

from proj.celery import app
import time
from celery.task import periodic_task
from celery.schedules import crontab
import datetime
import subprocess,os

@app.task(queue='PrioHigh', default_retry_delay=30, max_retries=5, ignore_result=True)
def RHigh(msg):
    try:
        subprocess.call(msg, shell=True, stdout=open(os.devnull, 'w'))
    except Exception as e:
        RHigh.retry(e)

@app.task(queue='PrioAver', default_retry_delay=300, max_retries=3, ignore_result=True)
def RAver(msg):
    try:
        subprocess.call(msg, shell=True, stdout=open(os.devnull, 'w'))
    except Exception as e:
        self.retry(e)

@app.task(queue='PrioLow', default_retry_delay=60, max_retries=0, ignore_result=True)
def RLow(msg):
    try:
        subprocess.call(msg, shell=True, stdout=open(os.devnull, 'w'))
    except Exception as e:
        self.retry(e)



