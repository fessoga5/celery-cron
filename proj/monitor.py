#!/usr/bin/env python2.7
import sys, os, time
sys.path.remove(os.path.dirname(__file__))

from celery import Celery
import os, subprocess, sys

from celery.task.control import inspect

def get_config_yaml(path_config=os.path.dirname(os.path.realpath(__file__)) + "/config.yaml", name_config="celery-cron"):
    import yaml
    from yaml import Loader, SafeLoader

    def construct_yaml_str(self, node):
        # Override the default string handling function 
        # to always return unicode objects
        return self.construct_scalar(node)

    Loader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)
    SafeLoader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)

    try:
        f = open(path_config)
    except:
        return dict({'user': 'guest', 'password': 'guest', 'host': 'localhost'})

    # use safe_load instead load
    dataMap = yaml.load(f)[name_config]
    f.close()
    return dataMap

app = Celery('proj',
             broker='amqp://{user}:{password}@{host}'.format(**get_config_yaml()),
             backend='amqp://{user}:{password}@{host}'.format(**get_config_yaml()),
             include=['proj.tasks'])


# curl -i -u admin:qeH6lvA4Vto6 "http://localhost:15672/api/queues/%2F/PrioLow"
def getSlavesNodes(user= "guest", password= "guest", host= ""):
    import json, urllib2, base64

    url = "http://localhost:15672/api/queues/%2F/PrioLow"
    request = urllib2.Request(url)
    base64string = base64.encodestring('%s:%s' % (user, password)).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)   

    data = json.load(urllib2.urlopen(request, timeout=1))

    return data['slave_nodes']

if __name__ == "__main__":
    print type(getSlavesNodes(**get_config_yaml()))

#i = inspect()
#
#c = list()
#
#for key, value  in  i.active().items():
#    for item in value:
#        c.append(str(item['args'][2:-3]))
#
#if sys.argv[1] in c:
#    print False
