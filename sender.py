#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#�| �~C�~A�~Aкий
# vim: sts=4 ts=4 sw=4 et ai

from celery import Celery
from proj.tasks import RHigh,RAver,RLow
from celery.result import AsyncResult
from celery.decorators import periodic_task
from celery.task.schedules import crontab
from celery.task.control import inspect
import time, sys
import subprocess
import os

import syslog
class CLOG():

    def __init__(self):
        pass

    def error(self, msg):
        syslog.syslog(syslog.LOG_ERR, msg)

    def critical(self, msg):
        syslog.syslog(syslog.LOG_CRIT, msg)

    def warning(self, msg):
        syslog.syslog(syslog.LOG_WARNING, msg)

    def info(self, msg):
        syslog.syslog(syslog.LOG_INFO, msg)

def get_config_yaml(path_config=os.path.dirname(os.path.realpath(__file__)) + "/proj/config.yaml", name_config="celery-cron"):
    import yaml
    from yaml import Loader, SafeLoader

    def construct_yaml_str(self, node):
        # Override the default string handling function 
        # to always return unicode objects
        return self.construct_scalar(node)

    Loader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)
    SafeLoader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)

    try:
        f = open(path_config)
    except:
        return dict({'user': 'guest', 'password': 'guest', 'host': 'localhost'})

    # use safe_load instead load
    dataMap = yaml.load(f)[name_config]
    f.close()
    return dataMap

# curl -i -u admin:qeH6lvA4Vto6 "http://localhost:15671/api/queues/%2F/PrioLow"
def getSlavesNodes(user= "guest", password= "guest", host= ""):
    import json, urllib2, base64

    url = "http://localhost:15671/api/queues/%2F/PrioLow"
    request = urllib2.Request(url)
    base64string = base64.encodestring('%s:%s' % (user, password)).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)   

    try:
        data = json.load(urllib2.urlopen(request, timeout=1))
        CLOG().info("Check slave_nodes - {} ".format(data['slave_nodes']))
        return data['slave_nodes']
    except Exception as e:
        CLOG().error("Don't check status slaves-nodes")
        return list()

def checkCurrentProcess(process):
    try:
        i = inspect()
        c = list()

        for key, value  in  i.active().items():
            for item in value:
                c.append(str(item['args'][2:-3]))

        if process not in c:
            return True
        else:
            return False
        
    except Exception as e:
        CLOG().error("Don't check current processes!")
        return True
        

if __name__ == "__main__":

    import socket
    master_node = "rabbit@{}".format(socket.gethostname())

    if len(sys.argv) > 1:
        if master_node not in getSlavesNodes(**get_config_yaml()):


            if checkCurrentProcess(sys.argv[1]): 
                taskO =  RLow.delay(sys.argv[1]).task_id
                CLOG().info("Command: \"{}\", started! ".format(sys.argv[1]))
            else:
                CLOG().warning("Command: \"{}\", already started! ".format(sys.argv[1]))
        else:
            CLOG().info("This is not master node! Cron don't start!")
    

